// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import com.google.gson.reflect.TypeToken;

import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;

public class HighscoreList
{
    private final static Type HIGHSCORE_LIST_TYPE = new TypeToken<ArrayList<HighscoreEntry>>(){}.getType();
    private final List<HighscoreEntry> highscoreList;
    private final StorageManager storageManager;

    public HighscoreList(final StorageManager storageManager, final List<HighscoreEntry> highscores) {
        this.storageManager = storageManager;

        if (highscores != null) {
            highscoreList = highscores;
        } else {
            highscoreList = new ArrayList<>();
        }
    }

    public static Type getHighscoreListType() {
        return HIGHSCORE_LIST_TYPE;
    }

    public void addNewHighscore(final String name, final int score) {
        highscoreList.add(new HighscoreEntry(name, score));
        highscoreList.sort(new HighscoreComparator());
        storageManager.storeHighscores(highscoreList);
    }

    public int getHighscoreListSize() {
        return highscoreList.size();
    }

    public List<HighscoreEntry> getHighscores(final int start, final int end) {
        return highscoreList.subList(start, end);
    }
}
