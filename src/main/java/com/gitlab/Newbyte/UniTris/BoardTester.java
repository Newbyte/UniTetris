// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import com.google.gson.JsonSyntaxException;

import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.UnsupportedAudioFileException;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.io.IOException;
import java.util.List;

public class BoardTester
{
    public static void main(final String[] args) {
		final ScoreManager scoreManager = new ScoreManager();
		final Board board = new Board(11, 17, scoreManager);
		scoreManager.addScoreListener(board);
		final StorageManager storageManager = new StorageManager();

		List<HighscoreEntry> highscores = null;

		highscoreRetryLoop: do {
		    try {
				highscores = storageManager.readHighscores();

				/*
				 * While it may look like this will make it never loop, keep in mind that we only
				 * get here if no exception is thrown (thus, the loop does continue if exceptions
				 * are thrown). Without this we get into an infinite loop if the highscore file
				 * does not yet exist since in that case the method returns null without throwing
				 * an exception.
				 */
				if (highscores == null) {
				    break;
				}
		    } catch (final IOException | JsonSyntaxException exception) {
		        final int choice = JOptionPane.showConfirmDialog(
		       			null,
						"Something went wrong while reading in stored highscores: "
							+ exception
							+ "\n\nDo you want to try again? Choosing no will clear your current highscore list."
							+ " Press cancel to exit."
				);

		        switch (choice) {
			    case 0:
			        break;
			    case 1:
			        break highscoreRetryLoop;
			    case 2:
			        return;
				}
		    }
		} while (highscores == null);

		try {
			new AudioManager();
		} catch (final LineUnavailableException | UnsupportedAudioFileException | IOException exception) {
			JOptionPane.showMessageDialog(
					null,
					"Something went wrong when setting up audio system: "
					+ exception
					+ "\n\nMusic will be disabled!"
			);
		}
		final HighscoreList highscoreList = new HighscoreList(storageManager, highscores);
		final JFrame frame = new JFrame();
		final TetrisComponent tetrisComponent = new TetrisComponent(board, highscoreList, frame, scoreManager);
		final JLabel scoreLabel = new JLabel();
		final TetrisViewer viewer = new TetrisViewer(tetrisComponent, scoreLabel, frame);
		scoreManager.addScoreListener(viewer);
		viewer.show();
		board.addBoardListener(tetrisComponent);
		final Action gameTick = new AbstractAction()
		{
		    @Override public void actionPerformed(final ActionEvent e) {
			board.tick();
		    }
		};

		final Timer gameTimer = new Timer(GameTimerManager.UPDATE_RATE_DEFAULT, gameTick);
		gameTimer.setCoalesce(true);
		gameTimer.start();
		final GameTimerManager gameTimerManager = new GameTimerManager(gameTimer);
		board.addBoardListener(gameTimerManager);
		scoreManager.addScoreListener(gameTimerManager);
    }
}
