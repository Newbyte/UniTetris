// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import static com.gitlab.Newbyte.UniTris.SquareType.*;

public class TetrominoMaker
{
    private final SquareType[] types = SquareType.values();
    private final static int NUM_INVALID_TYPES = 2;

    public int getNumberOfTypes() {
        return types.length - NUM_INVALID_TYPES;
    }

    public Poly getPoly(int n) {
        if (n > getNumberOfTypes()) {
            throw new IllegalArgumentException("Invalid index: " + n);
        }

        switch (n) {
            case 0:
                final SquareType[][] tetromino0 = {
                        {EMPTY, EMPTY, EMPTY, EMPTY},
                        {I, I, I, I},
                        {EMPTY, EMPTY, EMPTY, EMPTY},
                        {EMPTY, EMPTY, EMPTY, EMPTY}
                };
                return new Poly(tetromino0);
            case 1:
                final SquareType[][] tetromino1 = {
                        {J, EMPTY, EMPTY},
                        {J, J, J},
                        {EMPTY, EMPTY, EMPTY}
                };
                return new Poly(tetromino1);
            case 2:
                final SquareType[][] tetromino2 = {
                        {EMPTY, EMPTY, L},
                        {L, L, L},
                        {EMPTY, EMPTY, EMPTY}
                };
                return new Poly(tetromino2);
            case 3:
                final SquareType[][] tetromino3 = {
                        {O, O},
                        {O, O}
                };
                return new Poly(tetromino3);
            case 4:
                final SquareType[][] tetromino4 = {
                        {EMPTY, S, S},
                        {S, S, EMPTY},
                        {EMPTY, EMPTY, EMPTY}
                };
                return new Poly(tetromino4);
            case 5:
                final SquareType[][] tetromino5 = {
                        {EMPTY, T, EMPTY},
                        {T, T, T},
                        {EMPTY, EMPTY, EMPTY}
                };
                return new Poly(tetromino5);
            case 6:
                final SquareType[][] tetromino6 = {
                        {Z, Z, EMPTY},
                        {EMPTY, Z, Z},
                        {EMPTY, EMPTY, EMPTY}
                };
                return new Poly(tetromino6);
            default:
                throw new AssertionError("This should never happen");
        }
    }
}
