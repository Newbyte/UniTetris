// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import javax.swing.*;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.WindowEvent;
import java.util.List;

public class TetrisComponent extends JComponent implements BoardListener, KeyListener
{
    private final static int PIXELS_PER_BLOCK = 50;
    private final Board board;
    private final HighscoreList highscoreList;
    private final JFrame frame;
    private final ScoreManager scoreManager;

    public TetrisComponent(final Board board, final HighscoreList highscoreList, final JFrame frame, final ScoreManager scoreManager) {
        this.board = board;
        this.highscoreList = highscoreList;
        this.frame = frame;
        this.scoreManager = scoreManager;
        // Keyboard input doesn't work without this for some reason
        setFocusable(true);
        addKeyListener(this);
    }

    @Override public Dimension getPreferredSize() {
        return new Dimension(board.getWidth() * PIXELS_PER_BLOCK, board.getHeight() * PIXELS_PER_BLOCK);
    }

    @Override protected void paintComponent(final Graphics g) {
        super.paintComponent(g);
        final Graphics2D g2d = (Graphics2D) g;

        for (int y = 0; y < board.getHeight(); y++) {
            for (int x = 0; x < board.getWidth(); x++) {
                switch (board.getSquareAt(x, y)) {
                    case T:
                        g2d.setColor(Color.GREEN);
                        break;
                    case Z:
                        g2d.setColor(Color.RED);
                        break;
                    case S:
                        g2d.setColor(Color.BLUE);
                        break;
                    case O:
                        g2d.setColor(Color.YELLOW);
                        break;
                    case L:
                        g2d.setColor(Color.MAGENTA);
                        break;
                    case J:
                        g2d.setColor(Color.ORANGE);
                        break;
                    case I:
                        g2d.setColor(Color.CYAN);
                        break;
                    case EMPTY:
                        continue;
                }

                g2d.fillRect(x * PIXELS_PER_BLOCK, y * PIXELS_PER_BLOCK, PIXELS_PER_BLOCK, PIXELS_PER_BLOCK);
                g2d.setColor(Color.BLACK);
                g2d.drawRect(x * PIXELS_PER_BLOCK, y * PIXELS_PER_BLOCK, PIXELS_PER_BLOCK, PIXELS_PER_BLOCK);
            }
        }
    }

    @Override public void boardChanged() {
        repaint();
    }

    @Override public void gameOver() {
        System.out.println("Game over!");

        final String name = JOptionPane.showInputDialog(this, "Game over! Enter your name for the high score list");

        // We're draining the score outside of the if-case to ensure that you don't keep your score
        // for the next round of play if you decide to not add your highscore to the list
        final int finalScore = scoreManager.drainCurrentScore();

        if (name != null) {
            highscoreList.addNewHighscore(name, finalScore);
        }

        String[] options = {
                "Restart",
                "Quit",
                "View highscores"
        };

        final int choice = JOptionPane.showOptionDialog(
                this,
                "Game over! Play again?",
                "Game over!",
                JOptionPane.YES_NO_CANCEL_OPTION,
                JOptionPane.QUESTION_MESSAGE,
                null,
                options,
                options[2]
        );

        switch (choice) {
            case 0:
                board.reset();
                break;
            case 1:
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
                break;
            case 2:
                final List<HighscoreEntry> highscores = highscoreList.getHighscores(0, highscoreList.getHighscoreListSize());
                final StringBuilder highscoreTextBuilder = new StringBuilder();

                for (final HighscoreEntry highscore : highscores) {
                    highscoreTextBuilder.append(highscore.getName()).append(" - ").append(highscore.getScore()).append('\n');
                }

                JOptionPane.showConfirmDialog(this, highscoreTextBuilder.toString(), "Highscores", JOptionPane.DEFAULT_OPTION);
                board.reset();
                break;
        }
    }

    @Override public void keyTyped(final KeyEvent e) {}

    @Override public void keyPressed(final KeyEvent e) {
        switch (e.getKeyCode()) {
            case 32:
                board.tick();
                break;
            case 80:
                board.togglePause();
                break;
            case 39:
            case 68:
                board.move(Direction.RIGHT);
                break;
            case 37:
            case 65:
                board.move(Direction.LEFT);
                break;
            case 38:
            case 87:
                board.rotate(Direction.RIGHT);
                break;
            case 40:
            case 83:
                board.rotate(Direction.LEFT);
                break;
        }
    }

    @Override public void keyReleased(final KeyEvent e) {}
}
