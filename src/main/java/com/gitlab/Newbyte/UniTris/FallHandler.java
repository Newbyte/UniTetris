// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public interface FallHandler
{
    public boolean hasCollision(final Board board, final int xOld, final int yOld);
}
