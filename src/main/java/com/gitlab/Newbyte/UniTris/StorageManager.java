// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import com.google.gson.Gson;
import com.google.gson.JsonSyntaxException;

import javax.swing.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.List;

public class StorageManager
{
    private final static String SUBDIRECTORY = File.separator + "newbyte-tetris" + File.separator;
    private final static String HIGHSCORES_FILE = "highscores.json";
    private final static String TEMPORARY_SUFFIX = ".tmp";
    private final Gson gson = new Gson();
    private final String highscorePath;

    public StorageManager() {
        // TODO: Not entirely XDG-compliant — wrong default path
        final String xdgDataHome = System.getenv("XDG_DATA_HOME");
        final String dataDirectory;

        if (xdgDataHome != null) {
            dataDirectory = xdgDataHome + SUBDIRECTORY;
        } else {
            dataDirectory = System.getProperty("user.home") + SUBDIRECTORY;
        }

        highscorePath = dataDirectory + HIGHSCORES_FILE;

        // We need to manually create path to the data directory should it not exist
        // as writing files in a non-existent directory does not work with the
        // approach used in this class
        try {
            Files.createDirectories(Paths.get(dataDirectory));
        } catch (final IOException exception) {
            System.err.println("Error when instantiating com.gitlab.Newbyte.UniTris.StorageManager: " + exception);
            exception.printStackTrace();
        }
    }

    public List<HighscoreEntry> readHighscores() throws IOException, JsonSyntaxException {
        final Path highscorePathObj = Path.of(highscorePath);

        if (!Files.exists(highscorePathObj)) {
            return null;
        }

        return gson.fromJson(Files.readString(highscorePathObj), HighscoreList.getHighscoreListType());
    }

    public void storeHighscores(final List<HighscoreEntry> highscoreList) {
        final String highscoreTemporaryPath = highscorePath + TEMPORARY_SUFFIX;
        final String highscoreJson = gson.toJson(highscoreList);

        boolean writerHadError = false;

        try (final PrintWriter fileWriter = new PrintWriter(highscoreTemporaryPath)) {
            fileWriter.write(highscoreJson);
            writerHadError = fileWriter.checkError();
        } catch (final FileNotFoundException exception) {
            JOptionPane.showMessageDialog(null, "Error while storing highscores: " + exception);
        }

        if (writerHadError) {
            JOptionPane.showMessageDialog(null, "Something went wrong while writing high scores to disk");
        } else {
            try {
                Files.move(Paths.get(highscoreTemporaryPath), Paths.get(highscorePath), StandardCopyOption.REPLACE_EXISTING);
            } catch (final IOException exception) {
                JOptionPane.showMessageDialog(null,
                                              "Something went wrong while writing high scores to disk: " + exception);
            }
        }
    }
}
