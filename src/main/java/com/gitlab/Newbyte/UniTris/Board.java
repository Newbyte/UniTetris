// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.List;
import java.util.Random;

public class Board implements ScoreListener
{
    private final static Random RNG = new Random();
    private final static int MARGIN = 2;
    private final static int DOUBLE_MARGIN = MARGIN * 2;
    private final TetrominoMaker tetrominoMaker = new TetrominoMaker();
    private final List<BoardListener> listeners = new ArrayList<>();
    private final ScoreManager scoreManager;
    private final SquareType[][] squares;
    private final int width;
    private final int height;
    private final int widthWithMargin;
    private final int heightWithMargin;
    private FallHandler fallHandler = new DefaultFallHandler();
    private Poly currentlyFalling = null;
    private boolean gameOver = false;
    private boolean isPaused = false;
    private int currentlyFallingX;
    private int currentlyFallingY;

    public Board(final int width, final int height, final ScoreManager scoreManager) {
        this.width = width;
        this.height = height;
        this.scoreManager = scoreManager;
        widthWithMargin = width + DOUBLE_MARGIN;
        heightWithMargin = height + DOUBLE_MARGIN;
        squares = new SquareType[heightWithMargin][widthWithMargin];

        reset();
    }

    public void reset() {
        for (int y = 0; y < squares.length; y++) {
            for (int x = 0; x < squares[y].length; x++) {
                squares[y][x] = isWithinMargin(x, y, widthWithMargin, heightWithMargin) ? SquareType.EMPTY : SquareType.OUTSIDE;
            }
        }

        currentlyFalling = null;
        gameOver = false;

        notifyListeners(BoardNotifyType.BOARD_CHANGED);
    }

    public void togglePause() {
        isPaused = !isPaused;
    }

    public void addBoardListener(final BoardListener listener) {
        listeners.add(listener);
    }

    public SquareType getSquareAt(final int x, final int y) {
        final int adjustedX = x + MARGIN;
        final int adjustedY = y + MARGIN;

        if (isPositionWithinCurrentFalling(adjustedX, adjustedY)) {
            return currentlyFalling.getSquare(getTetrominoRelativeX(adjustedX), getTetrominoRelativeY(adjustedY));
        } else {
            return getSquareAtRaw(adjustedX, adjustedY);
        }
    }

    public SquareType getSquareAtRaw(final int x, final int y) {
        return squares[y][x];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }

    public int getCurrentlyFallingX() {
        return currentlyFallingX - MARGIN;
    }

    public int getCurrentlyFallingY() {
        return currentlyFallingY - MARGIN;
    }

    public int getCurrentlyFallingWidth() {
        return currentlyFalling.getWidth();
    }

    public int getCurrentlyFallingHeight() {
        return currentlyFalling.getHeight();
    }

    public int getBoardRelativeX(final int x) {
        return x + getCurrentlyFallingX();
    }

    public int getBoardRelativeY(final int y) {
        return y + getCurrentlyFallingY();
    }

    public SquareType getCurrentlyFallingSquareAt(final int x, final int y) {
        return currentlyFalling.getSquare(x, y);
    }

    public boolean isCurrentlyFallingNull() {
        return currentlyFalling == null;
    }

    /**
     * Checks if a column has any empty tiles starting at yStart and ending at the bottom of the board
     * @param xColumn This should not include the margin
     * @param yStart This should not include the margin
     * @return True if the column is connected to the bottom of the screen, false otherwise
     */
    public boolean columnConnectedToBottom(final int xColumn, final int yStart) {
        for (int y = yStart; y < getHeight() + MARGIN; y++) {
            if (getSquareAtIgnorePlayerIgnoreMargin(xColumn, y) == SquareType.EMPTY) {
                return false;
            }
        }

        return true;
    }

    public void pushColumn(final int x, final int yStart) {
        final SquareType type = getSquareAtIgnorePlayerIgnoreMargin(x, yStart);
        setSquareAtIgnoreMargin(x, yStart, SquareType.EMPTY);

        final int seekOffset = 1;

        /*
         * When we want to find a new location for the block that is to be moved down,
         * we don't just want to re-place it where it was previously as it wouldn't get
         * moved then. This will happen if we don't add 1 to yStart as then the first
         * element it looks at always will be where it places the block (as that was the
         * one we just removed), so skipping that element makes sure this won't happen.
         */
        for (int y = yStart + seekOffset; y < getHeight() + MARGIN; y++) {
            if (getSquareAtIgnorePlayerIgnoreMargin(x, y) == SquareType.EMPTY) {
                setSquareAtIgnoreMargin(x, y, type);
                return;
            }
        }
    }

    public void move(Direction direction) {
        if (isPaused) {
            return;
        }

        final int oldX = currentlyFallingX;

        switch (direction) {
            case LEFT -> {
                currentlyFallingX--;

                if (hasCollision(oldX, currentlyFallingY)) {
                    currentlyFallingX++;
                }
            }
            case RIGHT -> {
                currentlyFallingX++;

                if (hasCollision(oldX, currentlyFallingY)) {
                    currentlyFallingX--;
                }
            }
        }

        notifyListeners(BoardNotifyType.BOARD_CHANGED);
    }

    public void tick() {
        if (gameOver || isPaused) {
            return;
        }

        if (currentlyFalling != null) {
            moveFalling();
        } else {
            final Poly nextPoly = tetrominoMaker.getPoly(RNG.nextInt(tetrominoMaker.getNumberOfTypes()));
            setFalling(nextPoly);
        }

        notifyListeners(BoardNotifyType.BOARD_CHANGED);
    }

    public void rotate(final Direction direction) {
        if (currentlyFalling == null || isPaused) {
            return;
        }

        final int rotations;
        final Poly originalPoly = currentlyFalling;

        switch (direction) {
            // Rotating right three times is the same as rotating left
            case LEFT -> rotations = 3;
            case RIGHT -> rotations = 1;
            default -> throw new AssertionError("Unexpected value: " + direction);
        }

        Poly newPoly = null;

        for (int i = 0; i < rotations; i++) {
            newPoly = rotateRight();
        }

        currentlyFalling = newPoly;

        if (hasCollision(getCurrentlyFallingX(), getCurrentlyFallingY())) {
            currentlyFalling = originalPoly;
        }
    }

    private SquareType getSquareAtIgnorePlayer(final int x, final int y) {
        return getSquareAtIgnorePlayerIgnoreMargin(x + MARGIN, y + MARGIN);
    }

    private SquareType getSquareAtIgnorePlayerIgnoreMargin(final int x, final int y) {
        return squares[y][x];
    }

    private void setSquareAt(final int x, final int y, final SquareType squareType) {
        setSquareAtIgnoreMargin(x + MARGIN, y + MARGIN, squareType);
    }

    private void setSquareAtIgnoreMargin(final int x, final int y, final SquareType squareType) {
        squares[y][x] = squareType;
    }

    private Poly rotateRight() {
        final Poly polyToRotate = new Poly(new SquareType[currentlyFalling.getHeight()][currentlyFalling.getWidth()]);

        for (int r = 0; r < polyToRotate.getHeight(); r++) {
            for (int c = 0; c < polyToRotate.getWidth(); c++) {
                polyToRotate.setSquare(polyToRotate.getHeight() - 1 - r, c, currentlyFalling.getSquare(c, r));
            }
        }

        notifyListeners(BoardNotifyType.BOARD_CHANGED);

        return polyToRotate;
    }

    private void moveFalling() {
        final int oldY = getCurrentlyFallingY();

        currentlyFallingY++;

        if (hasCollision(getCurrentlyFallingX(), oldY)) {
            /*
             * If we have a collision, move the currently falling tetromino back up and
             * merge it into the board (then set it to null so it's replaced)
             */
            currentlyFallingY--;

            for (int y = 0; y < currentlyFalling.getHeight(); y++) {
                for (int x = 0; x < currentlyFalling.getWidth(); x++) {
                    final SquareType squareType = currentlyFalling.getSquare(x, y);

                    if (squareType != SquareType.EMPTY) {
                        squares[getCurrentlyFallingY() + y][getCurrentlyFallingX() + x] = squareType;
                    }
                }
            }

            currentlyFalling = null;

            final BitSet fullRows = getFullRows();

            /*
             * To reset any powerup the player might have
             * This must come before rowsRemoved(),
             * otherwise this overwrites any eventual powerups
             * as those happen when rowsRemoved() is called
             */
            fallHandler = new DefaultFallHandler();

            scoreManager.rowsRemoved(fullRows.cardinality());

            // Remove full rows and move down other rows
            for (int y = getHeight() - 1; y != 0; y--) {
                if (fullRows.get(y)) {
                    for (int x = 0; x < getWidth(); x++) {
                        setSquareAt(x, y, SquareType.EMPTY);
                    }

                    for (int y2 = y; y2 != 0; y2--) {
                        for (int x2 = 0; x2 < getWidth(); x2++) {
                            setSquareAt(x2, y2, getSquareAtIgnorePlayer(x2, y2 - 1));
                        }
                    }
                }
            }
        }
    }

    private BitSet getFullRows() {
        final BitSet matchedRows = new BitSet(getHeight());

        for (int y = 0; y < getHeight(); y++) {
            boolean nonMatchedColumn = false;

            for (int x = 0; x < getWidth(); x++) {
                if (getSquareAtIgnorePlayer(x, y) == SquareType.EMPTY) {
                    nonMatchedColumn = true;
                    // If we found one empty element it's safe to break out as
                    // the entire row can't be filled (i.e. non-empty) then
                    break;
                }
            }

            if (!nonMatchedColumn) {
                matchedRows.set(y, true);
            }
        }

        return matchedRows;
    }

    private void notifyListeners(final BoardNotifyType notifyType) {
        for (final BoardListener listener : listeners) {
            if (notifyType == BoardNotifyType.BOARD_CHANGED) {
                listener.boardChanged();
            } else {
                listener.gameOver();
            }
        }
    }

    private void setFalling(final Poly poly) {
        final int oldX = getCurrentlyFallingX();
        final int oldY = getCurrentlyFallingY();

        currentlyFalling = poly;
        currentlyFallingX = (width / 2) + MARGIN;
        currentlyFallingY = poly.getHeight() + MARGIN;

        if (hasCollision(oldX, oldY)) {
            gameOver = true;
            notifyListeners(BoardNotifyType.GAME_OVER);
        }
    }

    private boolean isPositionWithinCurrentFalling(final int x, final int y) {
        final int tetrominoRelativeX = getTetrominoRelativeX(x);
        final int tetrominoRelativeY = getTetrominoRelativeY(y);

        if (
                currentlyFalling == null
                || tetrominoRelativeX < 0
                || currentlyFalling.getWidth() <= tetrominoRelativeX
                || tetrominoRelativeY < 0
                || currentlyFalling.getHeight() <= tetrominoRelativeY
        ) {
            return false;
        }

        return currentlyFalling.getSquare(tetrominoRelativeX, tetrominoRelativeY) != SquareType.EMPTY;
    }

    private boolean hasCollision(final int xOld, final int yOld) {
        return fallHandler.hasCollision(this, xOld, yOld);
    }

    private boolean isWithinMarginArbitary(final int position, final int length) {
        return position >= MARGIN && position < length - MARGIN;
    }

    private boolean isWithinMargin(final int x, final int y, final int width, final int height) {
        return isWithinMarginArbitary(x, width) && isWithinMarginArbitary(y, height);
    }

    private int getTetrominoRelativeX(final int x) {
        return x - getCurrentlyFallingX();
    }

    private int getTetrominoRelativeY(final int y) {
        return y - getCurrentlyFallingY();
    }

    @Override public void scoreUpdated(final int value) {
        if (value % 500 == 0 && value != 0) {
            fallHandler = new Fallthrough();
        }
        if (value % 700 == 0 && value != 0) {
            fallHandler = new Heavy();
        }
    }
}
