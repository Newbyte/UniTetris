// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public class Position
{
    public final int x;
    public final int y;

    public Position(final int x, final int y) {
	this.x = x;
	this.y = y;
    }
}
