// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import java.util.Comparator;

public class HighscoreComparator implements Comparator<HighscoreEntry>
{
    @Override public int compare(final HighscoreEntry o1, final HighscoreEntry o2) {
        final int score1 = o1.getScore();
        final int score2 = o2.getScore();

        if (score1 == score2) {
            return 0;
        }

        return score1 > score2 ? -1 : 1;
    }
}
