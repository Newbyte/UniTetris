// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import javax.swing.*;
import java.awt.*;

public class SplashComponent extends JComponent
{
    private final ImageIcon splashImage = new ImageIcon(ClassLoader.getSystemResource("images/hello_world.png"));

    @Override public Dimension getPreferredSize() {
        return new Dimension(500, 500);
    }

    @Override protected void paintComponent(final Graphics gfx) {
        super.paintComponent(gfx);

        final Graphics2D gfx2d = (Graphics2D) gfx;

        gfx2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        splashImage.paintIcon(this, gfx, 50, 50);
    }
}
