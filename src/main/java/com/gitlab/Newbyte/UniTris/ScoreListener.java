// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public interface ScoreListener
{
    public void scoreUpdated(final int value);
}
