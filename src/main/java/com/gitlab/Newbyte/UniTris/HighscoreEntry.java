// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public class HighscoreEntry
{
    private final String name;
    private final int score;

    public HighscoreEntry(final String name, final int score) {
        this.name = name;
        this.score = score;
    }

    public String getName() {
        return name;
    }

    public int getScore() {
        return score;
    }
}
