// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

public class DefaultFallHandler extends GenericFallHandler
{
    @Override public boolean specialCondition(final Board board, final int x, final int y) {
	return board.getSquareAtRaw(board.getBoardRelativeX(x), board.getBoardRelativeY(y)) != SquareType.EMPTY;
    }
}
