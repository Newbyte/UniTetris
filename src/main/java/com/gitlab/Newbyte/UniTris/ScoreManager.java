// SPDX-License-Identifier: Zlib
package com.gitlab.Newbyte.UniTris;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class ScoreManager
{
    private final static Map<Integer, Integer> SCORE_MAP = Map.of(1, 100, 2, 300, 3, 500, 4, 800);
    private final List<ScoreListener> scoreListeners = new ArrayList<>();
    private int currentScore = 0;

    public void addScoreListener(final ScoreListener scoreListener) {
        scoreListeners.add(scoreListener);
        notifyListeners();
    }

    public void rowsRemoved(final int numRows) {
        final Integer scoreDelta = SCORE_MAP.get(numRows);

        // If there's no score value for the specified key, let's just return
        if (scoreDelta == null) {
            return;
	}

        currentScore += scoreDelta;

        notifyListeners();
    }

    public int drainCurrentScore() {
        final int currentScoreCopy = currentScore;
        currentScore = 0;
        notifyListeners();
        return currentScoreCopy;
    }

    private void notifyListeners() {
        scoreListeners.forEach((scoreListener -> scoreListener.scoreUpdated(currentScore)));
    }
}
